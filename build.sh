#!/bin/bash 

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUTw
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


for var in NEXUS_KANOD_USER NEXUS_KANOD_PASSWORD REPO_URL VERSION K8S_VERSION
do
    if [ -z "${!var}" ]; then
        echo "${var} must be defined"
        exit
    fi
done

set -eu

basedir=$(dirname "${BASH_SOURCE[0]}")
url="$REPO_URL"
format="${IMAGE_FORMAT:-raw}"
# shellcheck disable=SC2153
version="$VERSION"
groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')
artifactId="rke2-${K8S_VERSION}"

if ! curl --head --silent --output /dev/null --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.qcow2"; then
  echo "Building rke image"
  "${PYTHON:-/usr/bin/python3}" -m pip install diskimage-builder
  "${basedir}/make-image.sh" -t "${format}" -s "k8s_release=${K8S_VERSION}"
  if [ -f "rke2.${format}" ]; then
    # shellcheck disable=SC2086
    mvn ${MAVEN_CLI_OPTS} -B deploy:deploy-file ${MAVEN_OPTS} \
      -DgroupId="${groupId}" -DartifactId="${artifactId}" \
      -Dversion="${version}" -Dtype="${format}" -Dfile="rke2.${format}" \
      -DrepositoryId=kanod -Durl="${url}" \
      -Dfiles="${basedir}/rke2-schema.yaml" -Dtypes=yaml  -Dclassifiers=schema
    rm -f "rke2.${format}"
  else
    echo "Build for rke2 image failed"
  fi
fi
