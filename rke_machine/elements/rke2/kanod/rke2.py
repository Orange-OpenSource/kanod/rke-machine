#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
import subprocess
import time

from kanod_configure import common


def configure_rke2_server(arg: common.RunnableParams):
    conf = arg.conf
    proxy = conf.get('proxy', None)
    # We keep kanod standard naming for kubeadm
    kubernetes = conf.get('kubernetes', {})
    role = kubernetes.get('role', 'control')
    if role == 'control' or role == 'standalone':
        kind = 'server'
    elif role == 'worker':
        kind = 'agent'
    else:
        print('* role should be either control or worker.')
        return
    print(f'* Installing RKE2 in mode {kind}')
    env_install = {
        **os.environ,
        'INSTALL_RKE2_ARTIFACT_PATH': '/opt/rke2',
        'INSTALL_RKE2_TYPE': kind
    }
    t1 = time.time()
    subprocess.run(['/opt/rke2/install.sh'], env=env_install)
    t2 = time.time()
    print(f'* Launching RKE2 (install {t2 - t1})')
    if proxy is not None:
        with open(f'/etc/default/rke2-{kind}', 'w', encoding='utf-8') as fd:
            if 'http' in proxy:
                fd.write(f'HTTP_PROXY={proxy["http"]}\n')
            if 'https' in proxy:
                fd.write(f'HTTPS_PROXY={proxy["https"]}\n')
            if 'no_proxy' in proxy:
                fd.write(f'NO_PROXY={proxy["no_proxy"]}\n')
    subprocess.run(['systemctl', 'enable', f'rke2-{kind}', '--now'])

    if role == 'standalone':
        print(f'* copy kubeconfig in /home/admin')
        subprocess.run(['cp', '/etc/rancher/rke2/rke2.yaml',
                        '/home/admin/kubeconfig'])
        subprocess.run(['chown', 'admin:admin', '/home/admin/kubeconfig'])


common.register('RKE2 configuration', 200, configure_rke2_server)
