#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import re
import requests

from cloudinit import subp
from kanod_configure import common


def launch_keepalived(arg: common.RunnableParams):
    '''Activate keepalived

    Will only work with kubernetes on port 6443. For RKE the port cannot
    be moved so it is not a problem.
    '''
    k8s_vars = arg.conf.get('kubernetes', {})
    endpoint = k8s_vars.get('endpoint')
    keepalived = k8s_vars.get('keepalived', None)
    if keepalived is None:
        print('* not configured')
        return
    k8s_itf = keepalived.get('interface', None)
    router_id = keepalived.get('router_id', None)
    if router_id is None:
        address_re = re.compile(r'[0-9]*\.[0-9]*\.[0-9]*\.([0-9]*)')
        match = address_re.match(endpoint)
        if match is not None:
            router_id = match.group(1)
        else:
            router_id = '2'
    common.render_template(
        'keepalived.tmpl', 'etc/keepalived/keepalived.conf',
        {'k8s_itf': k8s_itf, 'endpoint': endpoint, 'router_id': router_id})
    try:
        requests.get(
            f'https://{endpoint}:6443/healthz',
            timeout=10, verify=False)
    except:    # noqa: disable=E722
        command = ['systemctl', 'start', 'keepalived']
        subp.subp(command)
    command = ['systemctl', 'enable', '--now', 'monitor-keepalived.service']
    subp.subp(command)


common.register('keepalived', 180, launch_keepalived)
